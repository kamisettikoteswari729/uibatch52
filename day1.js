console.log("hello world");
// about variables
//var,let and const
//var is used in global scope and functional scope
//let is used in block scope(within {}) and let keyword data can modify but dont try to redeclare
//const is used in block scope and we can modify the data(we can reassign the)
// variables are the containers and it is used to store the values in the memory
if (true) {
    var a=20;
    console.log(a);
}
//primitive data types
let name = "javascript";
let id =101;
let coursecompletion = true;
let job = null;
let batch;
console.log("hello");
console.log(typeof(name));
console.log(typeof(id));
console.log(typeof(coursecompletion));
console.log(typeof(job));
console.log(typeof(batch));

//complex data types
let scores=[34,"hello",98,99,72];
console.log(scores[1]);
console.log(scores[0]);
console.log(scores[2]);
console.log(scores.length);

//looping statements for loop,while loop,do while
for(let i=0;i<scores.length;i++){
    console.log(scores[i]);
}

//function-block of reusable code
function addition() {
    let result=30+40;
    return result;
}// function declaration

console.log(addition())  //function calling

function subtraction(num1,num2){
    return num1-num2
}
console.log(subtraction(50,10));

//let firstnum=prompt("enter the value");
//console.log(firstnum);

//let firstnumber=prompt("enter the value");
//alert("the value you have entered is"+firstnumber);
//console.log(firstnum);


function findmax(n1,n2) {
    return Math.max(n1,n2);
}
console.log(findmax(20,50));

function findmin(n1,n2,n3) {
    return Math.min(n1,n2,n3);
}
console.log(findmin(10,20,30));

//generate random number
let randomnumber=Math.round(Math.random()*100);
console.log( "randomnumber is: ",randomnumber);



console.log(Math.ceil(56.98));
console.log(Math.abs(45));
console.log(Math.pow(2,5));

var i=0;
var a=100;
console.log("all even numbers from 1 to 100");
for (i=1;i<=100;i++){
    if(i%2==0){
        console.log(i);
    }
}
console.log("all odd numbers from 1 to 100");
for(j=1;j<=100;j++){
    if(j%2!==0){
        console.log(j);
    }
}
var i=0;
var a=50;
console.log("all even numbers from 1 to 50");
for (i=1;i<=50;i++){
    if(i%2==0){
        console.log(i);
    }
}
console.log("all odd numbers from 1 to 50");
for(j=1;j<=50;j++){
    if(j%2!==0){
        console.log(j);
    }
}

